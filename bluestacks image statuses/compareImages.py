import cv2
import numpy as np
import sys

imagename1 = sys.argv[1]
imagename2 = sys.argv[2]

img1 = cv2.imread(imagename1, 0)
img2 = cv2.imread(imagename2, 0)

res = cv2.absdiff(img1, img2)

res = res.astype(np.uint8)

percentage = (np.count_nonzero(res) * 100) / res.size
print(percentage)
