﻿import pandas as pd  # openpyxl == 3.0.1
from bs4 import BeautifulSoup
from datetime import datetime
from tqdm import tqdm
import urllib.request
import pyperclip
import smtplib
import autoit
import time
import os



# Configurations
FIREFOX_URIS = [
	r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe',
	r'C:\Program Files\Mozilla Firefox\firefox.exe']

GOOGLE_APPS_URL = 'https://play.google.com/store'

GENERIC_PASSWORD_GOOGLE = 'vip5vip5'
HEB_PAGE = 'מיקום: ישראלשפה: עברית'
ENG_PAGE = 'Location: IsraelLanguage: English'
HEB_REVIEW_TAG = '<span class="G4lal" style="vertical-align: top;"> כתיבת ביקורת</span>'
ENG_REVIEW_TAG = '<span class="G4lal" style="vertical-align: top;"> Write a Review</span>'
ENG_REVIEW_LABEL_TAG = '<textarea class="WaMMDf" jsname="qzIxEb" jsaction="input:spUNYe,As2QGb;" maxlength="4096" placeholder="Tell others what you think about this app. Would you recommend it, and why?" aria-label="Tell others what you think about this app. Would you recommend it, and why?"></textarea>'
HEB_REVIEW_LABEL_TAG = '<textarea class="WaMMDf" jsname="qzIxEb" jsaction="input:spUNYe,As2QGb;" maxlength="4096" placeholder="כדאי לספר לאחרים מה חשבת על האפליקציה הזו. היית רוצה להמליץ עליה? למה, או למה לא?" aria-label="כדאי לספר לאחרים מה חשבת על האפליקציה הזו. היית רוצה להמליץ עליה? למה, או למה לא?"></textarea>'
APP_COMPATIBLE_WDEVICE_TAGS = ['<span>This app is compatible with all of your devices.</span>', '<span>This app is compatible with your device.</span>']
APP_NONE_COMPATIBLE_WDEVICE_TAG = '''<a href="https://support.google.com/googleplay/?p=remote_install_error" target="_blank">You don't have any devices.</a>'''
ACT_HELP = lambda action_range, action : [autoit.send('{%s}' % (action)) for i in range(action_range)]

# - - - - - bitbucket credentials - - - -
BITBUCKET_USER = 'sugarderryfire'
BITBUCKET_PASSWORD = 'bit5bit5!!'
# DB
PUSH_DB_COMMAND =   r'curl -u ' + BITBUCKET_USER + ':' + BITBUCKET_PASSWORD + ' -X POST https://api.bitbucket.org/2.0/repositories/sugarderryfire/reviewsproject/src/ -F reviewdb.xlsx=@reviewdb.xlsx'
REVIEW_DB_URL = r'https://bitbucket.org/sugarderryfire/reviewsproject/raw/master/reviewdb.xlsx'


# Goole Apps Issues
PHONE_ISSUE_URL = 'https://myaccount.google.com/signinoptions/recovery-options-collection?utm_source=Web&utm_medium=Web&utm_campaign=interstitial&oev=lytf%3D7%26wvtx%3D2%26trs%3Dli%26stel%3D1&hl=iw&continue=https://accounts.google.com/ServiceLogin?continue%3Dhttps%253A%252F%252Fplay.google.com%252Fstore%252Fapps%26hl%3Diw%26authuser%3D0%26passive%3Dtrue%26sarp%3D1%26aodrpl%3D1%26checkedDomains%3Dyoutube%26checkConnection%3Dyoutube%'
CONFIRM_ACCOUNT_URL = 'https://accounts.google.com/signin/v2/challenge/selection?passive=1209600&continue='
AFTER_PHONE_ISSUE_URL = 'https://myaccount.google.com/?utm_source=sign_in_no_continue'
SERVICE_LOGIN_URL = 'flowName=GlifWebSignIn&flowEntry=ServiceLogin'

# IP Changing
REBOOT_PAGE_URL = 'http://192.168.8.1/html/reboot.html'
MOBILE_WIFI_URL = 'http://192.168.8.1/' 
WIFI_LOGIN_CREDENTIALS = 'admin', 'admin123'


# TODO LIST: 
# 1. 
# 2. 
# 3. 
# 4. 

class Review:
	def __init__(self, app_id, key, curr_email, curr_recovery, review_time, curr_review, review_index):
		self.app_id = app_id
		self.key = key
		self.email = curr_email
		self.recovery = curr_recovery
		self.time = review_time
		self.review_txt = curr_review
		self.review_index = review_index

	def __str__(self):
		return '{0}|{1}|{2}'.format(self.review_index, self.email.rstrip(), self.recovery.lstrip())



# This function is reading and parsing a data from bitbucket. 
# The function will return list of Review's objects
def read_db():
	reviews = []
	try:
		excel_uri = REVIEW_DB_URL[-13:]
		urllib.request.urlretrieve(REVIEW_DB_URL, excel_uri)
		df = pd.read_excel(REVIEW_DB_URL, sheet_name='Sheet1')  # open the db
		for i in range(len(df)):
			
			if(df['done'][i] == 'no' and check_time(df['time'][i])):
				reviews.append(Review(df['appid'][i], df['keyword'][i], df['email'][i],
				df['recovery'][i], df['time'][i], df['review'][i], i))

		return reviews

	except Exception as e:
		print(e)


# This function will check if it's time to post a new review. (every review has its time to post.)
def check_time(review_time): 

	#2020-02-20 13:30
	review_time = datetime.strptime(str(review_time), '%Y-%m-%d %H:%M:%S')
	now = datetime.now()

	return review_time < now


# wrap function to prevent duplicate code 
def progress_bar_wrap(msg, minutes):

	for i in tqdm(range(60*minutes), ascii = True, desc = msg):
		time.sleep(1)


# Open excatcly the fit path of the system (pathes is constant)
def open_firefox():
	for path in FIREFOX_URIS:
		if(os.path.isfile(path)):
			autoit.run(path + ' -private-window') 
			break


#To prevent duplicates
def generic_handler(recovery_email):

	location = (814, 421)
	had_handle = False 


	while (True):

		autoit.send('{F5}')
		time.sleep(10)
		autoit.send('!d')
		time.sleep(2)
		autoit.send('^c')
		autoit.send('{ENTER}')
		time.sleep(5)

		if (CONFIRM_ACCOUNT_URL in pyperclip.paste()):
			print('# Handaling Account Confirmation')
			time.sleep(3)
			autoit.send('{TAB 3}')
			autoit.send('{ENTER}')
			time.sleep(3)
			autoit.send(recovery_email)
			autoit.send('{ENTER}')
			time.sleep(10)
			had_handle = True
		
		if (PHONE_ISSUE_URL in pyperclip.paste()):
			print('# Handling Phone Issue')

			time.sleep(3) 
			autoit.send('{TAB 2}')
			time.sleep(1)
			autoit.send('{ENTER}')
			time.sleep(10)
			had_handle = True

		if (AFTER_PHONE_ISSUE_URL == pyperclip.paste()):
			print('# Handling The After Phone Issue')

			time.sleep(3) 
			autoit.send('!d')
			time.sleep(2)
			autoit.send(GOOGLE_APPS_URL)
			autoit.send('{ENTER}')
			time.sleep(5)
			had_handle = True

		if(SERVICE_LOGIN_URL == pyperclip.paste()[-45:]):
			print('# Handling Service Login (Verify It\'s You) Issue')
			autoit.mouse_click('left', 814, 421) # Identify before installation
			time.sleep(5)
			autoit.send(GENERIC_PASSWORD_GOOGLE)
			autoit.send('{ENTER}')
			time.sleep(15)
			had_handle = True
		phone_number_verify_url = '&followup=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps&flowEntry=ServiceLogin'
		if phone_number_verify_url == pyperclip.paste()[-77:]:
			autoit.send('^w')
			return 'phone_number_issue'
		
		if(had_handle): had_handle = False
		else: return True
	

# Logging out and remove account from 'last account '
def logout():

	print('# Logging Out')
	# Logging out
	autoit.send('{F5}')
	time.sleep(3)
	autoit.send('{TAB 2}')
	autoit.send('{ENTER}')
	time.sleep(1)
	autoit.send('{TAB 3}')
	autoit.send('{ENTER}')
	time.sleep(4)	

	autoit.send('{TAB 3}')
	autoit.send('{ENTER}')
	time.sleep(3)	

	# Remove account from 'last accounts'
	autoit.send('{F5}')
	time.sleep(5)
	autoit.send('{TAB 3}')
	autoit.send('{ENTER}')
	time.sleep(2)
	autoit.send('{+ TAB}')
	autoit.send('{+ TAB}')
	autoit.send('{ENTER}')
	autoit.send('{ENTER}')
	print('# Log Out Was Finished Successfully')
	autoit.send('^w')

def send_email(reviewer_email):
	message = """From: {reviewer_email} <{reviewer_email}>
						To: sugar derry <sugarderryfire@gmail.com>
						Subject: #Phone Number Issue-messaget with {reviewer_email}

						Content: #Phone Number Issue
						Message: {reviewer_email} do not passed the log-in system because of the phone number issue, please pay attention""".format(reviewer_email=reviewer_email)

	mail_addr = 'sugarderryfire@gmail.com'
	pass_login = 'sugderfire5sugderfire5'

	server = smtplib.SMTP_SSL('smtp.googlemail.com', 465)
	server.login(mail_addr, pass_login)
	server.sendmail(mail_addr, mail_addr, message)

# Signing by email
def signin(review):

	time.sleep(5)
	autoit.mouse_click('left', 1285, 110)

	#autoit.send('{TAB 2}')
	#autoit.send('{ENTER}')
	time.sleep(8)
	autoit.send(review.email)
	autoit.send('{ENTER}')
	time.sleep(5)
	autoit.send(GENERIC_PASSWORD_GOOGLE)
	autoit.send('{ENTER}')
	time.sleep(7)

	if 'phone_number_issue' == generic_handler(review.recovery):
		print('** It\'s a Phone Number Issue!')
		send_email(review.email)
		progress_bar_wrap('#Sending US a Mail..', 5)
		update_excel(review)
		progress_bar_wrap('- Sleeping After Pushing Excel Into Bitbucket', 2)
		assert Exception
	
	
# Post a review
def review_logical(review, app_found, GENERATED_URL_APP):

	if(app_found == False):
		# It means that the app was not found in the search
		# then it will going manually and review the app
		time.sleep(8)
		autoit.send('!d')
		time.sleep(1)
		autoit.send(GENERATED_URL_APP)
		autoit.send('{ENTER}')
		time.sleep(5)

	location ,review_locations = {}, {}
	language = ''
	time.sleep(3)
	# autoit.send('^a ^c')
	# time.sleep(3)

	shit='''if(HEB_PAGE in pyperclip.paste()):
		location = {'installationX' : 475, 'installationY' : 459, 
					'confirm_instX' : 343, 'confirm_instY' : 560, 
					'reviewX' :  530, 'reviewY': 674,
					'identifyX' : 544, 'identifyY' : 430 }
		review_locations = {
					'review_buttonX' : 514, 'review_buttonY' : 212, 
					'midd_pageX' : 617, 'midd_pageY' : 354, 
					'starsX' : 606, 'starsY' : 564,
					'accept_reviewX' :  380, 'accept_reviewY' : 621}
		language = 'hebrew'
		'''


	# (ENG_PAGE in pyperclip.paste()):
	location = {'installationX' : 870, 'installationY' : 467, 
				'confirm_instX' : 1039, 'confirm_instY' : 510, 
				'reviewX' :  829, 'reviewY' : 589, 
				'identifyX' : 544, 'identifyY' : 430 }
	review_locations = {
				'review_buttonX' : 610, 'review_buttonY' : 337, 
				'midd_pageX' : 617, 'midd_pageY' : 354, 
				'starsX' : 729, 'starsY' : 556, 
				'accept_reviewX' :  962, 'accept_reviewY' : 617}
	language = 'english'
	# print('# The Language Is: ' + language)

	connect_device = {
				'inspect_X' : 16, 'inspect_Y' : 500,
				'check_device_X' : 488, 'check_device_Y' : 434}
	autoit.send('{F5}')
	time.sleep(8)
	autoit.send('^{HOME}')
	time.sleep(2)
	autoit.send('{F12}')
	time.sleep(1)
	autoit.mouse_click('left', connect_device['inspect_X'], connect_device['inspect_Y']) 
	time.sleep(1)
	autoit.mouse_click('left', connect_device['check_device_X'], connect_device['check_device_Y'])
	time.sleep(1)
	autoit.send('^c')
	time.sleep(1)
	autoit.send('{F12}')
	time.sleep(1)
	if pyperclip.paste() in APP_COMPATIBLE_WDEVICE_TAGS:
		print('# There Is a Device Connected!')
	else:
		#elif pyperclip.paste() == APP_NONE_COMPATIBLE_WDEVICE_TAG or 'You don\'t have any devices.' in pyperclip.paste():
		print('# There Is Not a Device Connected, Connecting...')
		update_excel(review, 'blue', 'yes')
		print('# Running Bluestacks Connector')
		os.system('python {}'.format(r'C:\Users\private\Desktop\reviewsproject\bluestacks_connector.py'))
		progress_bar_wrap('- After Connecting Device.', 5)
	
	
	print('## Installation!')
	autoit.send('{F5}')
	time.sleep(10)
	#autoit.mouse_click('left', 1333, 92)
	#time.sleep(10)
	autoit.mouse_click('left', location['installationX'], location['installationY'])
	time.sleep(10)
	autoit.mouse_click('left', location['confirm_instX'], location['confirm_instY'])
	time.sleep(4)

	generic_handler(review.recovery)

	autoit.send('{F5}')
	time.sleep(8)
	autoit.send(GENERIC_PASSWORD_GOOGLE)
	time.sleep(2)
	autoit.send('{ENTER}')
	time.sleep(2)
	autoit.send('{ENTER}')
	time.sleep(7)
	autoit.send('{F5}')
	time.sleep(7)

	if is_there_review(language):
		print('# Review Stage')
		if review_label_shown(review, language):
			if not review.review_txt:
				pyperclip.copy("review")
			else:
				pyperclip.copy(review.review_txt)

			time.sleep(2)
			autoit.mouse_click('left', review_locations['midd_pageX'], review_locations['midd_pageY'])
			autoit.send(pyperclip.paste())
			time.sleep(5)
			autoit.send('{TAB}')
			time.sleep(2)
			autoit.send('{RIGHT}')
			time.sleep(2)
			autoit.send('{RIGHT}')
			time.sleep(2)
			autoit.send('{RIGHT}')
			time.sleep(2)
			autoit.send('{RIGHT}')
			time.sleep(2)
			autoit.send('{SPACE}')
			#autoit.send({RIGHT})
			time.sleep(2)
			autoit.send('{TAB}')
			time.sleep(2)
			autoit.send('{TAB}')
			time.sleep(2)
			autoit.send('{SPACE}')
			#autoit.mouse_click('left', review_locations['starsX'], review_locations['starsY']) 
			#time.sleep(2)
			#autoit.mouse_click('left', review_locations['accept_reviewX'], review_locations['accept_reviewY']) 
			time.sleep(60)

	logout()
	

# The function will check if the user already left a review
# The checking process will be done via the web inspector
def is_there_review(language):
	REVIEW_TAG = ''
	if language == 'hebrew':
		locations = {
					'inspectorX' : 19, 'inspectorY' : 498, 
					'review_buttonX' : 431, 'review_buttonY' : 340, 
					'review_tagX' : 189, 'review_tagY' : 619
					}
		REVIEW_TAG = HEB_REVIEW_TAG

	elif language == 'english':
		locations = {
					'inspectorX' : 19, 'inspectorY' : 498, 
					'review_buttonX' : 245, 'review_buttonY' : 342, 
					'review_tagX' : 189, 'review_tagY' : 619
					}
		REVIEW_TAG = ENG_REVIEW_TAG

	if(not language):
		#raise exception
		pass

	autoit.send('{F5}')
	time.sleep(5)
	autoit.send('{DOWN 20}')
	time.sleep(3)

	autoit.send('{F12}')
	time.sleep(3)

	autoit.mouse_click('left', locations['inspectorX'], locations['inspectorY'])
	time.sleep(2)
	autoit.mouse_click('left', locations['review_buttonX'], locations['review_buttonY'])
	time.sleep(2)
	autoit.mouse_click('left', locations['review_tagX'], locations['review_tagY'])
	time.sleep(2)
	autoit.send('^c')
	time.sleep(2)
	autoit.send('{F12}')


	review_tag = BeautifulSoup(pyperclip.paste(), 'lxml')
	review_tag = str(review_tag.find_all(class_ = 'G4lal'))[1:-1]

	return review_tag.lower() == REVIEW_TAG.lower()


# Firefox new version prevent from us to know if review label 
# Is shown or maybe blank page is shown
def review_label_shown(review, language):

	REVIEW_LABEL_TAG = ''
	if language == 'hebrew':
		locations = {
				'review_buttonX' : 525, 'review_buttonY' : 272,
				'inspectorX' : 19, 'inspectorY' : 498, 
				'midd_pageX' : 673, 'midd_pageY' : 372, 
				'tag_checkX' : 139, 'tag_checkY' : 676, 
				'midd_labelX' : 617, 'midd_labelY': 371 }
		REVIEW_LABEL_TAG = HEB_REVIEW_LABEL_TAG

	elif language == 'english':
		locations = {
				'review_buttonX' : 812, 'review_buttonY' : 185,
				'inspectorX' : 19, 'inspectorY' : 498, 
				'midd_pageX' : 603, 'midd_pageY' : 207, 
				'tag_checkX' : 139, 'tag_checkY' : 676, 
				'midd_labelX' : 617, 'midd_labelY': 371 }
		REVIEW_LABEL_TAG = ENG_REVIEW_LABEL_TAG

	pyperclip.copy('')

	counter = 0
	while pyperclip.paste() != REVIEW_LABEL_TAG:
		counter +=1
		if(counter == 7):
			break
		autoit.send('{F5}')
		time.sleep(5)
		autoit.send('{PGUP 5}')
		time.sleep(5)
		autoit.send('{DOWN 20}')
		time.sleep(3)

		autoit.mouse_click('left', locations['review_buttonX'], locations['review_buttonY']) 
		time.sleep(2)

		autoit.send('{F12}')
		time.sleep(2)

		autoit.mouse_click('left', locations['inspectorX'], locations['inspectorY'])
		time.sleep(2)

		autoit.mouse_click('left', locations['midd_pageX'], locations['midd_pageY'])
		time.sleep(2)

		autoit.mouse_click('left', locations['tag_checkX'], locations['tag_checkY'])
		time.sleep(2)

		autoit.send('^c')
		time.sleep(2)

		if REVIEW_LABEL_TAG != pyperclip.paste():
			for i in range(4):
				autoit.send('{UP}')
				time.sleep(1)
				autoit.send('^c')
				time.sleep(2)

				if REVIEW_LABEL_TAG == pyperclip.paste():
					autoit.send('{F12}')
					return True


			#autoit.send('{ENTER}')
			autoit.mouse_click('left',  locations['midd_labelX'], locations['midd_labelY'])
			autoit.send('{F12}')

		else:
			autoit.send('{F12}')
			return True
	redirecting(review)


#The function check for the app in the search results
def app_in_result(review, GENERATED_URL_APP):
	locations = {'inspectorX' : 85, 'inspectorY' : 597, 
				 'midd_pageX' : 650, 'midd_pageY' : 262 }
	

	SEARCHING_URL = 'https://play.google.com/store/search?q={}&hl=en'.format(review.key)
	#SEARCHING_URL =SEARCHING_URL +'&hl=en'

	autoit.send('!d')
	time.sleep(1)
	autoit.send(SEARCHING_URL)
	time.sleep(2)
	autoit.send('{ENTER}')
	time.sleep(8)

	autoit.mouse_click('left', 500, 104)
	time.sleep(2)
	autoit.send('^a')
	time.sleep(1)
	autoit.send('{DEL}')
	time.sleep(2)


	autoit.mouse_click('left', locations['midd_pageX'], locations['midd_pageY'])
	time.sleep(2)

	for i in range(6):
		autoit.send('^{END}')
		time.sleep(2)
	time.sleep(3)

	app_found = False
	

	autoit.send('^f')
	time.sleep(3)
	autoit.send(review.key)
	time.sleep(1)
	autoit.send('{ENTER}')
	copied_links = []
	for i in range(35):
		autoit.send('{ESC}')
		time.sleep(1)
		autoit.send('+{F10}')
		time.sleep(1)
		autoit.send('{DOWN 6}')
		time.sleep(1)
		autoit.send('{ENTER}')

		copied_link = pyperclip.paste()
		copied_link = copied_link.replace('https://play.google.com/store/apps/', '')
		if copied_link.startswith('details?id='):
			if copied_links not in copied_links:
				copied_links.append(pyperclip.paste())
			else:
				print('# App Was Not Founded In The Results')
				return False

		if pyperclip.paste() != GENERATED_URL_APP[:-6]:
			autoit.send('^f')
			time.sleep(2)
			autoit.send('{ENTER 2}')
		else:
			print('# App Was Found In The Results')
			autoit.send('{ENTER}')
			progress_bar_wrap('Watch The Page', 1)
			return True	


	print('# App Was Not Founded In The Results')
	return app_found
	

def redirecting(review):
	REDIRECT_REVIEW_URL = 'https://play.google.com/store/ereview?origin=https://play.google.com&docId=<APP_ID>&hl=en&width=1366&source=play-store-boq-web&usegapi=1&id=I5_1582048407285&_gfid=I5_1582048407285&parent=https://play.google.com&pfname&rpctoken=13419157&jsh=m;/_/scs/abc-static/_/js/k%3Dgapi.gapi.en.Sj5LKyeUKoE.O/d%3D1/ct%3Dzgms/rs%3DAHpOoo9ToCtoaz0mr9IKXAop6Eq9AIpSlw/m%3D__features__'
	REDIRECT_INSTALLATION_URL = 'https://tokenized.play.google.com/eacquire/app?authuser=0&ear=%5B%5B%22com.sugar.powerfulquotes%22%2C7%5D%2Cnull%2C%5Bnull%2Cnull%2C%22%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.sugar.powerfulquotes%22%5D%2Cnull%2C%5B%22W1tudWxsLG51bGwsbnVsbCxbbnVsbCxbIlJzMi4wLjY6OjpzMTEsMiwyNmI1ZTE5LDEsNTU2LGEwNjNlYmU5LDAsMzAwLGVkZDk4YmFjLDAsMTgsNDg2M2ZkMzUsMCw1NTYsY2IyZDVjNmYsMCwyZDgsNmFkNDdjNmMsMCw1NTYsN2JkYjQ5ZjYsMCwyOGUsYjY1NDAyMDAsMCw1NjYsZWVhODIwYjYsMCwyZTgsMWFhNDMzMSwxLFwiV2luMzIsYWY3OTQ1MTUsMCxcIjUuMCAyOFdpbmRvd3MyOSxkODE3MjNkMSwwLFwiZW4yZFVTLDVjYzNhYjVmLDAsXCJNb3ppbGxhMmY1LjAgMjhXaW5kb3dzIE5UIDEwLjAzYiBXaW42NDNiIHg2NDNiIHJ2M2E3My4wMjkgR2Vja28yZjIwMTAwMTAxIEZpcmVmb3gyZjczLjAsMjRhNjZkZjYsMCwtNzgsXCJUaHUgSmFuIDAxIDE5NzAgMDIzYTAwM2EwMCBHTVQyYjAyMDAgMjhJc3JhZWwgU3RhbmRhcmQgVGltZTI5LDc3MGM2N2ZjLDAsMjphMjEsMywxNzA1OTgyNzZjYiwwLDg0LDI6YTQwLFwiZiwxNzA1OTgyNzZjZCJdLCJNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjo3My4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94LzczLjAiLCJodHRwczovL3BheW1lbnRzLmdvb2dsZS5jb20iLG51bGwsW10sbnVsbCxudWxsLG51bGwsbnVsbCxudWxsLG51bGwsW11dLG51bGwsbnVsbCwiZW4iLDEsMSxudWxsLG51bGwsbnVsbCxudWxsLG51bGwsW10sW10sbnVsbCxudWxsLFtdXSxbImh0dHBzOi8vcGF5bWVudHMuZ29vZ2xlLmNvbS9wYXltZW50cy9yZWRpcmVjdF9mb3JtX2xhbmRpbmc%2Fc3VjY2Vzcz10cnVlIiwiaHR0cHM6Ly9wYXltZW50cy5nb29nbGUuY29tL3BheW1lbnRzL3JlZGlyZWN0X2Zvcm1fbGFuZGluZz9zdWNjZXNzPWZhbHNlIixudWxsLFtdXSxbXV0%3D%22%5D%2Cnull%2C%5B%5B%22CAE%3D%22%5D%2Cfalse%5D%2C0%5D&plpeid=1582048351195&usegapi=1&id=I1_1582049687260&_gfid=I1_1582049687260&parent=https%3A%2F%2Fplay.google.com&pfname=&rpctoken=36128341&jsh=m%3B%2F_%2Fscs%2Fabc-static%2F_%2Fjs%2Fk%3Dgapi.gapi.en.Sj5LKyeUKoE.O%2Fd%3D1%2Fct%3Dzgms%2Frs%3DAHpOoo9ToCtoaz0mr9IKXAop6Eq9AIpSlw%2Fm%3D__features__'
	
	autoit.send('!d')
	time.sleep(1)
	autoit.send(REDIRECT_REVIEW_URL.replace('<APP_ID>', review.app_id))
	time.sleep(3)
	autoit.send('{ENTER}')
	time.sleep(8)
	autoit.send('^a')
	time.sleep(1)
	autoit.send(review.review_txt)
	time.sleep(4)
	autoit.send('{TAB}')
	time.sleep(1)
	autoit.send('{UP}')
	time.sleep(2)
	autoit.send('{TAB 2}')
	time.sleep(2)
	autoit.send('{ENTER}')
	time.sleep(2)
	time.sleep(5)
	autoit.send('^w')


# This function will connect into google apps account via autoit
def google_apps_scenario(review):

	GENERATED_URL_APP = 'https://play.google.com/store/apps/details?id={}&hl=en'.format(review.app_id) 
	try:

		open_firefox()
		time.sleep(3)
		autoit.send('!d')
		time.sleep(1)
		autoit.send(GOOGLE_APPS_URL)
		time.sleep(3)	
		autoit.send('{ENTER}')
		signin(review)	

		# Searching for the app in the search bar
		time.sleep(5)
		app_found = app_in_result(review, GENERATED_URL_APP)
		# GENERATED_URL_APP = GENERATED_URL_APP+'&hl=en'
		review_logical(review, app_found, GENERATED_URL_APP)

		time.sleep(5)
			

	except Exception as e:
		print(e)
		autoit.send('^w')
		progress_bar_wrap('- Trying Again Within A Minute', 1)
		

		google_apps_scenario(review)


# The function changes to 'yes' so it wont repeat on the review
# - Dowloading the xlsx file
# - Change the file by the review index of the iteration
# - Push it to the bit brunch
def update_excel(review, row='done', value='yes'):

	excel_uri = REVIEW_DB_URL[-13:]
	urllib.request.urlretrieve(REVIEW_DB_URL, excel_uri)

	dframe = pd.read_excel(excel_uri)

	dframe.iloc[review.review_index, dframe.columns.get_loc(row)] = value
	writer = pd.ExcelWriter(excel_uri)  # write to the given excel file
	dframe.to_excel(writer, 'Sheet1', index=False)  # writing the existing content
	writer.save()

	print('# Pushing The DB Into Bitbucket ...')
	if row == 'done':
		push_command = PUSH_DB_COMMAND + ' -F "message=Finished To Review {app} From {r_mail}"'.format(app=review.app_id, r_mail=review.email)
	elif row == 'blue':
		push_command = PUSH_DB_COMMAND + ' -F "message=Updating Excel For Bluestacks-Connector to connect {r_mail}"'.format(r_mail=review.email)
	else:
		push_command = PUSH_DB_COMMAND

	push_status = os.system(push_command)
	time.sleep(8)
	if(not push_status): # operation succeeded
		print('# Pushing Finished Successfully')

	
# This function will take care to the syncronization of the data's db and the current review
def config_fllow():

	for review in read_db(): # Looping over the reviews list
	
		print('# Current Review Starting Now:')
		print('# The Email Of The User Is:', review.email)
		print('# The Key Word Of The App Will Review Is:', review.key)

		google_apps_scenario(review)

		try:
			progress_bar_wrap('- Sleeping Before Pushing Excel Into Bitbucket', 2)

		except KeyboardInterrupt as e:
			print('* If You Want The Branch\'s Excel Will Be Up To Date So You Have To Wait!')

		update_excel(review)
		progress_bar_wrap('- Sleeping Before Changing IP', 10)

		change_ip()
		progress_bar_wrap('- Sleeping After IP Was Changed', 60)


		print();print();print();print()


# Connecting to NetStick portal and changes the ip
def change_ip():
	locations = {'x_login_button' : 1110, 'y_login_button' : 90, 
				 'x_login_enter' : 729, 'y_login_enter' : 498, 
				 'x_reboot_button' : 1081, 'y_reboot_button' : 349, 
				 'x_reboot_confirmation' : 795, 'y_reboot_confirmation' : 453, 
				 'x_exit' : 1344, 'y_exit' : 19 }

	open_firefox()
	time.sleep(3)
	autoit.send('!d')
	autoit.send(MOBILE_WIFI_URL)
	time.sleep(3)
	autoit.send('{ENTER}')
	time.sleep(5)

	
	# Login 
	print('# Changing The ip')
	autoit.mouse_click('left', locations['x_login_button'], locations['y_login_button'])
	time.sleep(3)
	autoit.send(WIFI_LOGIN_CREDENTIALS[0])
	time.sleep(1)
	autoit.send('{TAB}')
	autoit.send(WIFI_LOGIN_CREDENTIALS[1])
	time.sleep(1)
	autoit.mouse_click('left', locations['x_login_enter'], locations['y_login_enter'])
	autoit.send('{ENTER}')
	time.sleep(5)

	# Changing the ip address
	autoit.send('!d')
	autoit.send(REBOOT_PAGE_URL)
	time.sleep(3)
	autoit.send('{ENTER}')
	time.sleep(10)
	autoit.mouse_click('left', locations['x_reboot_button'], locations['y_reboot_button'])
	time.sleep(3)
	autoit.mouse_click('left', locations['x_reboot_confirmation'], locations['y_reboot_confirmation'])
	time.sleep(60)
	autoit.mouse_click('left', locations['x_exit'],  locations['y_exit'])
	time.sleep(5)
	autoit.mouse_click('left', locations['x_exit'],  locations['y_exit'])
	print('# IP Was Successfully Changed')


def main():
	
	while(True):
		print('# Starting A New Iteration...')
		print('# The Time Now Is {}'.format(datetime.now()))

		if(read_db()): config_fllow()
		else: print('* Nothing Added To The Schehme, Waiting...')

		progress_bar_wrap('- Time Until Next Table Checking', 60)
		print();print();print();print()



if __name__ == '__main__':
	main()



