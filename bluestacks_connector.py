import pandas as pd
import time
import os
import autoit
import urllib.request
import cv2
from PIL import ImageGrab
import numpy as np
import openpyxl
from tqdm import tqdm

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

"""
this program run iteratively and do as follows for each gmail address in the file 'reviewdb.xlsx'
that have been pulled from bitbucket repository:
    1. install blutstacks emulator.
    2. log in to blue stacks using gmail address.
    3. after login in successfully, the program will close bluestacks.
    4. uninstall bluestacks.
    5. report to 'reviewdb.xlsx' if succeeded to login to bluestacks.
    6. push the updated 'reviewdb.xlsx' to bitbucket repository if succeeded to login to bluestacks.
    7. wait 20 minutes and continue to the next gmail address.
"""

GMAILS_PASSWORD = "vip5vip5"

# - - - - - email credentials - - - - - -
EMAIL_USER = 'sugarderryfire@gmail.com'
EMAIL_PASS = 'sugderfire5sugderfire5'
# - - - - - bitbucket credentials - - - -
BITBUCKET_USER = 'sugarderryfire'
BITBUCKET_PASSWORD = 'bit5bit5!!'
# - - - - - github credentials - - - -
GIT_USER = 'cyghtinc'
GIT_PASS = 'git5git5'
GIT_TOKEN = '3a8dd5b5887a2ff4613add0f16b6bb052b3ccd23'

# - - - - - - - - - - - - - - - - - - - -

installation_file_name = 'BlueStacksInstaller_4.205.0.1006_native.exe'
installation_file_path = r'C:\Users\private\Desktop\reviewsproject\BlueStacksInstaller_4.205.0.1006_native.exe'  # os.path.abspath(installation_file_name)

BLUESTACKS_RUN_PATH = r'C:\ProgramData\BlueStacks\client\Bluestacks.exe'

BLUESTACKS_UNINSTALL_PATH = [r'C:\Program Files (x86)\BlueStacks\BlueStacksUninstaller.exe',
                             r'C:\Program Files\BlueStacks\BlueStacksUninstaller.exe']

REVIEW_DB_bitbucket = r'https://bitbucket.org/sugarderryfire/reviewsproject/raw/master/reviewdb.xlsx'
DOWNLOAD_EXCEL_GIT = "curl  -L https://github.com/cyghtinc/reviewdb.xlsx/blob/master/reviewdb.xlsx?raw=true --output reviewdb.xlsx"

PUSH_COMMAND_TO_bitbucket = r'curl -u ' + BITBUCKET_USER + ':' + BITBUCKET_PASSWORD + ' -X POST https://api.bitbucket.org/2.0/repositories/sugarderryfire/reviewsproject/src/ -F reviewdb.xlsx=@reviewdb.xlsx'

DOWNLOAD_EXCEL_GIT = "curl  -L https://github.com/cyghtinc/reviewdb.xlsx/blob/master/reviewdb.xlsx?raw=true --output reviewdb.xlsx"

IMG_verify_email_address = cv2.imread('bluestacks image statuses/verifyEmailAddress.png', 0)
IMG_verify_add_phone_number = cv2.imread('bluestacks image statuses/addPhoneNumber.png', 0)
IMG_verify_add_phone_number_2 = cv2.imread(
    'bluestacks image statuses/addPhoneNumber_version2.png', 0)
IMG_add_phone_number_sceurity = cv2.imread(
    'bluestacks image statuses/addNumberToYouAccountSecurity.png', 0)
IMG_authorized = cv2.imread('bluestacks image statuses/authorized.png', 0)
IMG_authorized_2 = cv2.imread('bluestacks image statuses/authorized2.png', 0)
IMG_authorized_3 = cv2.imread('bluestacks image statuses/authorized3.png', 0)

IMG_installation_finished = cv2.imread('bluestacks image statuses/finish_installation.png', 0)
IMG_installation_finished_2 = cv2.imread('bluestacks image statuses/finish_installation_2.png', 0)
IMG_try_another_way_to_login = cv2.imread(
    'bluestacks image statuses/chooseVerificationMethod.png', 0)
IMG_enter_recovery_mail = cv2.imread('bluestacks image statuses/enter_recovery_mail.png', 0)
IMG_password_screen = cv2.imread('bluestacks image statuses/password_screen.png', 0)

click_coordinates = {'x_click_install': 688, 'y_click_install': 548, 'x_click_X_for_new_version': 868,
                     'y_click_X_for_new_version': 292,
                     'x_launch_btn': 681, 'y_launch_btn': 544, 'x_welcome_sign_in': 832, 'y_welcome_sign_in': 493,
                     'x_full_size_window': 1174, 'y_full_size_window': 65, 'x_sign_in': 649, 'y_sign_in': 531,
                     'x_email_field_focus': 587, 'y_email_field_focus': 447, 'x_email_next_btn': 1015,
                     'y_email_next_btn': 594, 'x_password_field_focus': 697, 'y_password_field_focus': 439,
                     'x_password_next_btn': 1012, 'y_password_next_btn': 575, 'x_add_phone_number_skip_btn': 252,
                     'y_add_phone_number_skip_btn': 609, 'x_authorization_i_agree_btn': 1013,
                     'y_authorization_i_agree_btn': 698,
                     'x_authorization_backup_btn': 1208, 'y_authorization_backup_btn': 573,
                     'x_authorization_more_btn': 1180,
                     'y_authorization_more_btn': 674, 'x_authorization_confirm_btn': 1180,
                     'y_authorization_confirm_btn': 674, 'x_recovery_mail_field_focus': 583,
                     'y_recovery_mail_field_focus': 556}


def send_email(current_email_address):
    """
    the function send an email with an attachment of a picture why the login process didnt succeeded.
    """
    email_subject = 'bluestacks tool failed connecting to ' + current_email_address
    msg = MIMEMultipart()
    msg['FROM'] = EMAIL_USER
    msg['To'] = EMAIL_PASS
    msg['Subject'] = email_subject
    body = 'bluestacks tool failed connecting to ' + current_email_address + '.\nreason in the attachment image.'
    msg.attach(MIMEText(body, 'plain'))
    filename = 'testImage.png'
    attachment = open(filename, 'rb')
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= " + filename)
    msg.attach(part)
    text = msg.as_string()
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(EMAIL_USER, EMAIL_PASS)

    server.sendmail(EMAIL_USER, EMAIL_USER, text)
    server.quit()


def wait_progress_bar(msg, minutes):
    """
    a progress bar for the waiting between the iterations
    """
    for i in tqdm(range(60 * minutes), ascii=True, desc=msg):
        time.sleep(1)


def wait_for_next_iteration():
    print('waiting 20 minutes between next iteration')
    time.sleep(300)
    print('15 minutes to go')
    time.sleep(300)
    print('10 minutes to go')
    time.sleep(300)
    print('5 minutes to go')
    time.sleep(60)
    print('4 minutes to go')
    time.sleep(60)
    print('3 minutes to go')
    time.sleep(60)
    print('2 minutes to go')
    time.sleep(60)
    print('1 minutes to go')


def add_email_to_black_list(current_email_address):
    """
    the functions adds a new mail to the black list.
    """
    with open(r'C:\Users\private\Desktop\reviewer-GITVERSION\reviewer\blacklist.csv', 'a+', newline='') as fd:
        fd.write(current_email_address + '\n')


def check_if_already_logged_in_with_this_mail(data_frame, current_email_address):
    """
    the method checks of the given current email address already committed login to bluestacks.
    """
    for current_record_index in range(len(data_frame)):
        data_record = data_frame.iloc[current_record_index]
        if data_record['email'] == current_email_address and data_record['blue'] == 'no':
            return True
    return False


def check_if_bluestack_installed():
    """
    the function checks if blusestacks exist when initialing the program. if it does delete it.
    """
    if os.path.exists(BLUESTACKS_RUN_PATH):
        uninstall_bluestacks()


def get_file_xl_from_bitbucket():
    excel_name = REVIEW_DB_bitbucket[-13:]
    urllib.request.urlretrieve(REVIEW_DB_bitbucket, excel_name)
    data_frame = pd.read_excel(REVIEW_DB_bitbucket, sheet_name='Sheet1')
    return data_frame


def get_file_xl_from_git():
    os.system(DOWNLOAD_EXCEL_GIT)
    os.system('xcopy /y reviewdb.xlsx ..\\reviewdb.xlsx')  # also move it also in the repo dir
    dframe = pd.read_excel('reviewdb.xlsx', sheet_name='Sheet1')
    return dframe


def push_back_to_bitbucket(current_email_address):
    """
    the function pulls from bitbucket repository the latest updated reviewdb file and update
    on it on the given gmail address that it succeeded to log in to bluestacks.
    """

    # first pull from repository the latest updated reviewdb file.

    updated_data_frame = get_file_xl_from_git()

    # get the row number of the gmail address the program just committed a login successfully
    index_list = updated_data_frame[updated_data_frame['email'] == current_email_address].index.to_numpy()

    for current_index in index_list:
        current_record = updated_data_frame.iloc[current_index]
        if current_record['blue'] == 'yes':  # checks if which record marked as needed to commit login to bluestacks.
            write_to_xl_reviewdb(current_index + 2)
            break


def install_bluestacks_installation():
    """
    the function initial the installation of bluestacks.
    """
    if not os.path.exists(installation_file_path):
        print('[bluestacks] ' + installation_file_path + ' Not exist')
        return

    autoit.run(installation_file_path)
    is_installation_window_loaded = False
    while not is_installation_window_loaded:
        try:
            autoit.win_activate('BlueStacks Installer')
            is_installation_window_loaded = True
        except:
            time.sleep(2)

    time.sleep(6)
    autoit.mouse_click('left', click_coordinates['x_click_install'], click_coordinates['y_click_install'])
    time.sleep(0.1)
    autoit.mouse_click('left', click_coordinates['x_click_install'], click_coordinates['y_click_install'])
    time.sleep(1)
    autoit.mouse_click('left', click_coordinates['x_click_install'], click_coordinates['y_click_install'])
    time.sleep(8)


def wait_for_installation_to_finish_and_launch():
    """
    the function checks every 30 seconds if the installation is finished
    """
    # - - - - - wait for installation to complete - - - -
    is_installation_finished = False
    while not is_installation_finished:
        try:
            autoit.win_activate('BlueStacks Installer')
            current_installation_status_image = take_photo_of_bluestacks()
            similarity = compare_similarity_between_2_images(IMG_installation_finished,
                                                             current_installation_status_image)
            similarity_2 = compare_similarity_between_2_images(IMG_installation_finished_2,
                                                               current_installation_status_image)
            if similarity < 0.1 or similarity_2 < 0.1:
                is_installation_finished = True
            time.sleep(7)
        except:
            time.sleep(7)

    # - - - - - click on 'Launch' button - - - -
    time.sleep(4)
    autoit.win_activate('BlueStacks Installer')
    time.sleep(0.5)
    autoit.mouse_click('left', click_coordinates['x_launch_btn'], click_coordinates['y_launch_btn'])
    time.sleep(0.1)
    autoit.mouse_click('left', click_coordinates['x_launch_btn'], click_coordinates['y_launch_btn'])
    time.sleep(0.1)
    autoit.mouse_click('left', click_coordinates['x_launch_btn'], click_coordinates['y_launch_btn'])


def open_bluestacks():
    """
    the function runs bluestacks
    """
    is_bluestacks_opened = False
    while not is_bluestacks_opened:
        time.sleep(3)
        try:
            autoit.run(BLUESTACKS_RUN_PATH)
            time.sleep(10)
            autoit.win_activate('BlueStacks')
            is_bluestacks_opened = True
        except:
            time.sleep(0.0001)


def wait_for_update_to_finish():
    """
    the function checks if the updates of bluestacks as finished.
    """

    # wait for 'update version' windows to appear and than click X.
    is_updates_finished = False
    while not is_updates_finished:
        try:
            autoit.win_activate('ContainerWindow')
            time.sleep(2)
            autoit.mouse_click('left', click_coordinates['x_click_X_for_new_version'],
                               click_coordinates['y_click_X_for_new_version'])
            is_updates_finished = True
            time.sleep(2)
        except:
            time.sleep(0.0001)

    # wait for google log-in pop up to appear.
    is_updates_finished = False
    while not is_updates_finished:
        try:
            autoit.win_activate('ContainerWindow')
            time.sleep(8)
            is_updates_finished = True
        except:
            time.sleep(0.0001)


def authorization_click():
    # click on "i agree"
    autoit.mouse_click('left', click_coordinates['x_authorization_i_agree_btn'],
                       click_coordinates['y_authorization_i_agree_btn'])
    time.sleep(10)

    # click for NOT 'Back up to Google Drive'
    autoit.mouse_click('left', click_coordinates['x_authorization_backup_btn'],
                       click_coordinates['y_authorization_backup_btn'])
    time.sleep(2)

    # click on 'more'
    autoit.mouse_click('left', click_coordinates['x_authorization_more_btn'],
                       click_coordinates['y_authorization_more_btn'])
    time.sleep(1)

    # click 'confirm'
    autoit.mouse_click('left', click_coordinates['x_authorization_confirm_btn'],
                       click_coordinates['y_authorization_confirm_btn'])
    time.sleep(1)


def choose_recovery_email_option():
    """
    the function focus on the recovery email field and enter
    the recovery email and click 'Next'
    """
    time.sleep(6)
    autoit.send('{TAB}')
    time.sleep(0.5)
    autoit.send('{ENTER}')


def enter_google_login(current_email_address, current_recovery_email):
    """
    the function initial the login process to bluestacks by gmail address

    """
    print('[bluestacks] Starting Login Process')
    # choose by clicking sign-in in google pop-up
    autoit.mouse_click('left', click_coordinates['x_welcome_sign_in'], click_coordinates['y_welcome_sign_in'])

    # # change to fulle-size windows
    autoit.mouse_click('left', click_coordinates['x_full_size_window'], click_coordinates['y_full_size_window'])
    time.sleep(15)

    # choose by clicking sign in
    autoit.mouse_click('left', click_coordinates['x_sign_in'], click_coordinates['y_sign_in'])

    time.sleep(60)
    autoit.win_activate('BlueStacks')
    # focus on the email address field box
    autoit.mouse_click('left', click_coordinates['x_email_field_focus'], click_coordinates['y_email_field_focus'])
    time.sleep(3)
    autoit.send(current_email_address)  # insert the current mail address
    time.sleep(2)

    # click on 'Next' button
    autoit.mouse_click('left', click_coordinates['x_email_next_btn'], click_coordinates['y_email_next_btn'])

    time.sleep(30)
    # focus on the 'enter password' field box
    autoit.mouse_click('left', click_coordinates['x_password_field_focus'], click_coordinates['y_password_field_focus'])
    time.sleep(1)
    autoit.send(GMAILS_PASSWORD)  # insert the password
    time.sleep(12)
    # click on 'Next' button
    autoit.send('{ENTER}')

    # if check_if_passed_password_screen() is False:
    #     print('[bluestacks] Failed Passing Password Screen.')
    #     return False

    time.sleep(3)

    # take a photo of the current status of BlueStacks.
    current_status_bluestacks_img = take_photo_of_bluestacks()

    # compare similarities to all windows states bluetacks can arrive.
    similarity_add_phonenumber, similarity_add_phone_number_2, similarity_add_phone_security, similarity_try_another_way_to_sign_in = check_state_similarity(
        current_status_bluestacks_img)

    # this value will use us if bluestacks arrived to 'try_another_way_to_sign_in' and because of that no need to choose recovery mail option.
    is_needed_choose_recovery_email_option = True
    if similarity_try_another_way_to_sign_in < 0.9:
        """
        if arrived here we will choose email verification option
        and we will continue to email verification.
        that why we take a picture again of bluestacks and compare similarity
        """
        print('[bluestacks] Arrived verification options window.')
        autoit.send('{TAB 4}')
        time.sleep(0.5)
        autoit.send('{ENTER}')
        time.sleep(8)

        current_status_bluestacks_img = take_photo_of_bluestacks()
        similarity_add_phonenumber, similarity_add_phone_number_2, similarity_add_phone_security, similarity_try_another_way_to_sign_in = check_state_similarity(
            current_status_bluestacks_img)
        is_needed_choose_recovery_email_option = False

    # print("similarity at phone number window: {}".format(similarity))
    if similarity_add_phonenumber < 1:
        print('[bluestacks] ' + current_email_address + " Arrived to 'Add phone number' window")
        # print("similarity at phone number window: {}".format(similarity))
        autoit.send('{TAB 4}')
        time.sleep(0.5)
        autoit.send('{ENTER}')
        time.sleep(3)

        authorization_click()

        return True

    elif similarity_add_phone_security < 0.9:
        print('[bluestacks] ' + current_email_address + " Arrived to 'Add Phone Number For Account Security' window")

        # click 'skip'
        autoit.mouse_click('left', click_coordinates['x_add_phone_number_skip_btn'],
                           click_coordinates['y_add_phone_number_skip_btn'])

        time.sleep(3)
        authorization_click()
        return True

    else:
        current_status_bluestacks_img = take_photo_of_bluestacks()
        similarity_email_verification = compare_similarity_between_2_images(IMG_verify_email_address,
                                                                            current_status_bluestacks_img)

        similarity_authorized = compare_similarity_between_2_images(IMG_authorized, current_status_bluestacks_img)
        similarity_authorized_2 = compare_similarity_between_2_images(IMG_authorized_2, current_status_bluestacks_img)
        similarity_authorized_3 = compare_similarity_between_2_images(IMG_authorized_3, current_status_bluestacks_img)
        similarity_enter_recovery_mail = compare_similarity_between_2_images(IMG_enter_recovery_mail,
                                                                             current_status_bluestacks_img)

        # print("similarity at email verification window: {0}".format(similarity))
        # print("similarity at email verification: {}".format(similarity_email_verification))

        if similarity_email_verification < 0.9 or similarity_enter_recovery_mail < 0.9 or similarity_authorized_3 < 0.9:  # arrived email verification

            if is_needed_choose_recovery_email_option:
                choose_recovery_email_option()

            time.sleep(5)
            autoit.send('{TAB}')
            time.sleep(0.5)
            autoit.send(current_recovery_email)
            time.sleep(0.5)
            autoit.send('{ENTER}')

            time.sleep(3)  # checks if arrived to add phone number window
            current_status_bluestacks_img = take_photo_of_bluestacks()
            similarity_add_phone_number = compare_similarity_between_2_images(IMG_verify_add_phone_number,
                                                                              current_status_bluestacks_img)
            similarity_add_phone_number_2 = compare_similarity_between_2_images(IMG_verify_add_phone_number_2,
                                                                                current_status_bluestacks_img)

            similarity_add_phone_security = compare_similarity_between_2_images(IMG_add_phone_number_sceurity,
                                                                                current_status_bluestacks_img)

            if similarity_add_phone_number < 0.9 or similarity_add_phone_number_2 < 0.9:
                print('[bluestacks] Arrived Add phone number window')
                autoit.send('{TAB 4}')
                time.sleep(0.5)
                autoit.send('{ENTER}')

                authorization_click()
                return True
            if similarity_add_phone_security < 0.9:
                # click 'skip'
                autoit.mouse_click('left', click_coordinates['x_add_phone_number_skip_btn'],
                                   click_coordinates['y_add_phone_number_skip_btn'])

                time.sleep(3)
                authorization_click()
                return True

        elif similarity_authorized < 0.9 or similarity_authorized_2 < 0.9:  # email is authorized
            authorization_click()
            return True

        else:  # need cellphone verification or accound disabled.
            print('[bluestacks] ' +
                  current_email_address + ' Arrived cellphone verification or it disabled. cant continue to login to this email address.')
            return False

        # check if succeeded logging in or if the account is disabled:
        # take a photo of the current status of BlueStacks.
        time.sleep(5)
        current_status_bluestacks_img = take_photo_of_bluestacks()

        similarity_2 = compare_similarity_between_2_images(IMG_authorized, current_status_bluestacks_img)
        similarity_3 = compare_similarity_between_2_images(IMG_authorized_2, current_status_bluestacks_img)
        similarity_4 = compare_similarity_between_2_images(IMG_authorized_3, current_status_bluestacks_img)

        if similarity_2 > 0.6 and similarity_3 > 0.6 and similarity_4 > 0.6:  # account disabled
            print(
                '[bluestacks] ' + current_email_address + " is disabled. cant continue to login to this email address.")
            return False

    authorization_click()
    return True


def check_if_passed_password_screen() -> bool:
    """
    The function checks if the program passed the password screen.
    if not
    """
    for x in range(5):
        current_status_bluestacks_img = take_photo_of_bluestacks()
        similarity = compare_similarity_between_2_images(IMG_password_screen, current_status_bluestacks_img)
        if similarity > 0.9:
            time.sleep(3)
            return True
        time.sleep(5)
        autoit.mouse_click('left', click_coordinates['x_password_next_btn'], click_coordinates['y_password_next_btn'])
        time.sleep(5)
    return False


def check_state_similarity(current_status_bluestacks_img):
    """
    the function checks similarity to all states the bluestacks can reach after
    inserting the gmail address and password of the current gmail account.
    """
    # checks if arrived to 'Add phone number' window
    similarity_add_phonenumber = compare_similarity_between_2_images(IMG_verify_add_phone_number,
                                                                     current_status_bluestacks_img)

    similarity_add_phone_number_2 = compare_similarity_between_2_images(IMG_verify_add_phone_number_2,
                                                                        current_status_bluestacks_img)

    # checks if arrived to 'Add phone number For Account Security' window
    similarity_add_phone_security = compare_similarity_between_2_images(IMG_add_phone_number_sceurity,
                                                                        current_status_bluestacks_img)

    # checks if arrived to 'try another way to sign in' window
    similarity_try_another_way_to_sign_in = compare_similarity_between_2_images(IMG_try_another_way_to_login,
                                                                                current_status_bluestacks_img)

    return similarity_add_phonenumber, similarity_add_phone_number_2, similarity_add_phone_security, similarity_try_another_way_to_sign_in


def take_photo_of_bluestacks():
    """
    the function takes a photo of the bluestacks windows and save it as 'testImage' in the program folder for later uses.
    """
    autoit.win_activate('BlueStacks')
    time.sleep(10)

    success = False
    current_bluestacks_status_img = None
    while not success:
        try:
            for i in range(10):
                autoit.send('!{PRINTSCREEN}')

            time.sleep(0.5)
            img_save = ImageGrab.grabclipboard()
            img_save.save("testImage.png", 'PNG')

            # get the saved photo
            current_bluestacks_status_img = cv2.imread('testImage.png', 0)
            success = True
        except:
            success = False

    return current_bluestacks_status_img


def compare_similarity_between_2_images(original_image, comparing_img):
    """
    the function compare similarity between 2 images of bluestacks.
    the program use it to check if the program arrived the a certain status or not
    by checking if the similarity is close enough.
    * * * NOTE: the lower the similarity percentage is close to 0% the, the higher the similarity between the 2 images.
    """
    # compare similarity between 2 images
    show_message = False
    res = None
    for x in range(5):
        try:
            res = cv2.absdiff(original_image, comparing_img)
            if show_message:
                print("[bluestacks] Succeeded comparing images")
            break
        except:
            print(
                "[bluestacks] Attempt number", x + 1, "out of 5 attempts to compare images failed. Trying again..")
            show_message = True
            comparing_img = take_photo_of_bluestacks()

    # get similarity by percentages - the lower the outcome the higher the similarity between the 2 images.
    res = res.astype(np.uint8)
    percentage = (np.count_nonzero(res) * 100) / res.size

    return percentage


def write_to_xl_reviewdb(row_index):
    """
    the function reports to the csv file report if succeeded logging-in to bluestacks or not.
    :param row_index: int. the given row number index in the reviewdb file to update if the login succeeded.
    """
    xl = openpyxl.load_workbook('reviewdb.xlsx')
    ws = xl.worksheets[0]
    ws.cell(row=row_index, column=7).value = 'no'
    xl.save('reviewdb.xlsx')


def uninstall_bluestacks():
    """
    the function uninstall blue stacks from the computer.
    """
    for path in BLUESTACKS_UNINSTALL_PATH:
        if os.path.isfile(path):
            autoit.run(path)
            time.sleep(5)
            autoit.win_activate('Bluestacks Uninstaller')
            autoit.send('{TAB 3}')
            time.sleep(3)
            autoit.send('{ENTER}')  # click on uninstall button
            time.sleep(3)

            # click uninstall again when asked 'are you sure?'

            success = False
            while not success:
                try:
                    autoit.win_activate('CustomMessageWindow')
                    autoit.send('{TAB 2}')
                    autoit.send('{ENTER}')
                    success = True
                except:
                    success = False

            success = False
            while not success:
                try:
                    time.sleep(4)
                    autoit.win_activate('Bluestacks Uninstaller')
                    autoit.send('{TAB}')
                    time.sleep(1)
                    autoit.send('{ENTER}')
                except:
                    success = True


def main():
    check_if_bluestack_installed()
    # read file csv from bitbucket
    data_frame = get_file_xl_from_git()  # get_file_xl_from_bitbucket()

    for current_record_index in range(len(data_frame)):
        data_record = data_frame.iloc[current_record_index]

        # check if needed to commit log in to this current gmail address.
        if data_record['blue'] == "no" and check_if_already_logged_in_with_this_mail(data_frame, data_record['email']):
            continue

        # check if already tried to login unsuccessfully to this email address.
        black_list_df = pd.read_csv('blacklist.csv')
        if data_record['email'] in black_list_df.values:
            continue

        print("\n[bluestacks] Working on : " + data_record['email'])

        # install bluestacks
        install_bluestacks_installation()

        wait_for_installation_to_finish_and_launch()

        open_bluestacks()

        wait_for_update_to_finish()

        # pull the gmail address and the recovery gmail address.
        current_email_address = data_record['email']
        current_recovery_email = data_record['recovery']

        # initial logging in to bluestacks
        is_succeeded = enter_google_login(current_email_address, current_recovery_email)

        if is_succeeded:
            print('[bluestacks] Succeeded login in to ', data_record['email'])
            push_back_to_bitbucket(current_email_address)
            os.system(PUSH_COMMAND_TO_bitbucket)
        else:
            send_email(current_email_address)
            # add the email to the black list
            add_email_to_black_list(current_email_address)

        time.sleep(4)
        uninstall_bluestacks()

        # wait 45 minutes between each iteration.
        print("[bluestacks] Finished working on : " + data_record['email'])
        wait_progress_bar('[bluestacks] Wait For Next Iteration', 45)


main()
